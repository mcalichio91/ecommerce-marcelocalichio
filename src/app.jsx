import NavBar from './components/NavBar/NavBar'
import CatalogoContainer from './layout/CatalogoContainer'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import ProductContainer  from './layout/ProductContainer';
function App() {
  return (
    <BrowserRouter>
      <NavBar></NavBar>
      <div className="App">
        <Routes>
          <Route path="*" element={<CatalogoContainer/>}/>
          <Route path="/categoria/:categoria" element={<CatalogoContainer/>}/>
          <Route path="/producto/:productoId" element={<ProductContainer/>}/>
        </Routes>
      </div>
    </BrowserRouter>
    );
}

export default App;