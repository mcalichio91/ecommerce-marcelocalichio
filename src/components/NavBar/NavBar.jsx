import './NavBar.scss'
import * as React from 'react';
import { useEffect, useState } from 'react';
import rickandmortyApi from '../../helpers/getCharacters';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Grid from '@mui/material/Grid'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'

 export default function Header () {

    return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static">
                    <Toolbar>
                    <IconButton
                        size="large"
                        edge="start"
                        color="inherit"
                        aria-label="menu"
                        sx={{ mr: 2 }}
                    >
                        {/* <MenuIcon /> */}
                    </IconButton>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        ECOMMERCE-MCALICHIO
                    </Typography>
                    <Grid>
                        <List>
                            <ListItem>
                                    {/* {
                                        species.map((el, key) => {
                                            return(
                                                <ListItemText key={key}>
                                                    {el.species}
                                                </ListItemText>
                                            )
                                        })
                                    } */}
                            </ListItem>
                        </List>
                    </Grid>
                    <Button color="inherit">Login</Button>
                    </Toolbar>
                </AppBar>
            </Box>
            
        </>
    );
 }