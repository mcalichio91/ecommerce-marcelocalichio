import * as React from 'react';
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import { Link } from 'react-router-dom';


const bull = (
  <Box
    component="span"
    sx={{ display: 'inline-block', mx: '2px', transform: 'scale(0.8)' }}
  >
    •
  </Box>
);

export default function Catalogo({dataItem}) {
  return (
      <Card sx={{ minWidth: 275 }}>
        <CardContent>
          <Grid container justifyContent="center">
            <Box
              component="img"
              sx={{
                height: 233,
                width: 'max-content',
              }}
              src={dataItem.image}
            />
          </Grid>
          
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {dataItem.id}
          </Typography>
          <Typography variant="h5" component="div">
            {dataItem.name}
          </Typography>
          <Typography sx={{ mb: 1.5 }} color="text.secondary">
            {dataItem.type}
          </Typography>
          <Typography variant="body2">
            {dataItem.status}
            <br />
            {'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit consectetur vero suscipit mollitia accusantium repudiandae totam libero temporibus nostrum.'}
          </Typography>
        </CardContent>
        <CardActions>
          <Button>
            <Link size="small" to={`/producto/${dataItem.id}`}>Detalle</Link>
          </Button>
        </CardActions>
      </Card>
  );
}