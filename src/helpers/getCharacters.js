const rickandmortyApi = {

    getCharacters: async function () {
        return (await fetch('https://rickandmortyapi.com/api/character')
        .then((res) => res.json())
        .then((data) => data.results)
        .catch(err => console.log("Error al obtener los datos:", err)))
    },

    getCharacter: async function (characterId) {
         return (await fetch(`https://rickandmortyapi.com/api/character/${characterId}`)
        .then((res) => res.json())
        .then((data) =>  data)
        .catch(err => console.log("Error al obtener los datos:", err)))

    }
}

export default rickandmortyApi;