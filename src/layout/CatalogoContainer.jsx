import React, { useEffect, useState } from 'react'
import Grid from '@mui/material/Grid';
import Catalogo from '../components/catalogo/Catalogo'
import rickandmortyApi from '../helpers/getCharacters';
import './catalogoContainer.scss'

export default function CatalogoContainer () {

    const [listCharacters, setCharacters] = useState([]) 
    
    useEffect(() => {
        async function getCharacters() {
            let data = await rickandmortyApi.getCharacters()
            setCharacters(data)            
        }

        getCharacters()
    }, [listCharacters])

    return (
        <>   
            <Grid container spacing={3} mt={1} className="flex-container">
                {
                    listCharacters.map((res) => (
                        <Grid item xs={3} key={res.id} >
                            <Catalogo dataItem={res} />
                        </Grid>
                    ))
                }
            </Grid>
        </>
    )
}