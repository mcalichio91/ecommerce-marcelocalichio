import { useEffect, useState } from "react";
import { useParams } from "react-router-dom"
import rickandmortyApi from "./../helpers/getCharacters";
import Producto  from "../components/producto/Producto";

export default function  ProductContainer () {
    const  { productoId } = useParams([]);
    const [character, setCharacter] = useState({});

    useEffect(() => {
        function getCharacter() {
        try {
                rickandmortyApi.getCharacter(productoId).then( data => setCharacter(data))
            }
        
        catch(err) {
            console.log(err)
        }
        }
        getCharacter()
    }, [productoId])

    return (
        <>
            <div>

                        <Producto dataProduct={character}/>

            </div>
        </>
    )
}